from typing import NamedTuple

import carl


def clamp(value, min_value, max_value):
    return max(min_value, min(value, max_value))


def read_int(path: str) -> int:
    with open(path) as f:
        return int(f.read())


class Device(NamedTuple):
    brightness: str
    max_brightness: str
    up_delta: int
    down_delta: int

    def write_brightness(self, amount: int):
        max_brightness = self.read_max_brightness()
        new = clamp(amount, 0, max_brightness)

        with open(self.brightness, 'w') as f:
            print(new, file=f, flush=True)

    def read_max_brightness(self):
        return read_int(self.max_brightness)

    def read_brightness(self):
        return read_int(self.brightness)

    def change_brightness(self, delta: int):
        brightness = self.read_brightness()
        self.write_brightness(brightness + delta)

    def up_brightness(self):
        self.change_brightness(self.up_delta)

    def down_brightness(self):
        self.change_brightness(self.down_delta)

    def at_least(self, amount: int):
        brightness = self.read_brightness()
        self.write_brightness(max(brightness, amount))

    def at_most(self, amount: int):
        brightness = self.read_brightness()
        self.write_brightness(min(brightness, amount))


def make_subcommands(name: str, device: Device, command):
    @command.subcommand([name])
    def sub(subcommand=None):
        if subcommand is None:
            print(device.read_brightness())

    sub.subcommand(['up'])(device.up_brightness)
    sub.subcommand(['down'])(device.down_brightness)
    sub.subcommand(['max'])(
        lambda: device.write_brightness(device.read_max_brightness())
    )
    sub.subcommand(['min'])(lambda: device.write_brightness(0))
    sub.subcommand(['get-max'])(lambda: print(device.read_max_brightness()))

    sub.subcommand(['at-most'])(device.at_most)
    sub.subcommand(['at-least'])(device.at_least)

    sub.subcommand(['value'])(device.write_brightness)


DEVICES = {
    'screen': Device(
        '/sys/class/backlight/intel_backlight/brightness',
        '/sys/class/backlight/intel_backlight/max_brightness',
        500,
        -500,
    ),
    'keyboard': Device(
        '/sys/class/leds/samsung::kbd_backlight/brightness',
        '/sys/class/leds/samsung::kbd_backlight/max_brightness',
        1,
        -1,
    ),
}


@carl.command
def main(subcommand):
    pass


for name, device in DEVICES.items():
    make_subcommands(name, device, main)


def run_main():
    main.run()


if __name__ == '__main__':
    run_main()
